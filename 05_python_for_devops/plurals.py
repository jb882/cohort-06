import sys
import re

arg=sys.argv

word=str(arg[1])


def pluralize(wrd):
    if re.search('[sxz]$', wrd):
        return re.sub('$', 'es', wrd)
    elif re.search('[^aeioudgkprt]h$', wrd):
        return re.sub('$', 'es', wrd)
    elif re.search('[^aeiou]y$', wrd):
        return re.sub('y', 'ies', wrd)
    else:
        return wrd + 's'
print(word, '=', pluralize(word))


