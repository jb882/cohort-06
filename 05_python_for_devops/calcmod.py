# This is the calculator module
def calculate(a, b, operator):
    a = float(a)
    b = float(b)
    try:
        if operator == '+':
            return a + b
        elif operator == '-':
            return a - b
        elif operator == '*':
            return a * b
        elif operator == '/':
            return a / b
        else:
            print('bad operator!', operator)
            return None
    except ZeroDivisionError:
        return 'Can not divied by zero....'
