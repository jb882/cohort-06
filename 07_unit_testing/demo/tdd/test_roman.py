import pytest
from roman import roman_to_decimal


def test_roman_one():
    roman_numeral = 'I'
    decimal_numeral = roman_to_decimal(roman_numeral)

    assert decimal_numeral == 1


def test_roman_five():
    roman_numeral = 'V'
    decimal_numeral = roman_to_decimal(roman_numeral)

    assert decimal_numeral == 5


def test_roman_two():
    roman_numeral = 'II'
    decimal_numeral = roman_to_decimal(roman_numeral)

    assert decimal_numeral == 2

def test_roman_ten():
    roman_numeral = 'X'
    decimal_numeral = roman_to_decimal(roman_numeral)

    assert decimal_numeral == 10

def test_roman_fifty():
    roman_numeral = 'L'
    decimal_numeral = roman_to_decimal(roman_numeral)

    assert decimal_numeral == 50

def test_roman_one_hundred():
    roman_numeral = 'C'
    decimal_numeral = roman_to_decimal(roman_numeral)

    assert decimal_numeral == 100

def test_roman_five_hundred():
    roman_numeral = 'D'
    decimal_numeral = roman_to_decimal(roman_numeral)

    assert decimal_numeral == 500

def test_roman_one_thousand():
    roman_numeral = 'M'
    decimal_numeral = roman_to_decimal(roman_numeral)

    assert decimal_numeral == 1000

def test_roman_six():
    roman_numeral = 'VI'
    decimal_numeral = roman_to_decimal(roman_numeral)

    assert decimal_numeral == 6
    
def test_roman_nine():
    roman_numeral = 'IX'
    decimal_numeral = roman_to_decimal(roman_numeral)

    assert decimal_numeral == 9

def test_roman_invalid_character():
    roman_numeral = 'Q'
    with pytest.raises(ValueError):
        decimal_numeral = roman_to_decimal(roman_numeral)

def test_roman_empty_string():
    roman_numeral = ''
    with pytest.raises(ValueError):
        decimal_numeral = roman_to_decimal(roman_numeral)
