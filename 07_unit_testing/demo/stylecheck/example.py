import random


def show_random():
    i = random.randint(1, 100)
    print("Random number:", i)
    return i


def Say_Hello(name):
    print("Hello,", name)


def main():
    show_random()
    Say_Hello("JR")


main()
