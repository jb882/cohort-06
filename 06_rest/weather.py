import pprint
import requests


lat= 42.200330
lon= -71.429640
SECRET_KEY = '1d8c58ed1d54f96f939e706c788650f1'

#lat, long = (42.200330, -71.429640) 

url = 'https://api.darksky.net/forecast/{key}/{lat},{long}'.format(key=SECRET_KEY, lat=lat, long=lon)
response = requests.get(url)
pprint.pprint(response.text)